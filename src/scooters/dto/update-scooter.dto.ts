import { IsNotEmpty } from 'class-validator';

export class UpdateScooterDto {
    @IsNotEmpty()
    readonly scootId: string;
    readonly batteryLevel: number;
    readonly longitude: number;
    readonly latitude: number;
}
