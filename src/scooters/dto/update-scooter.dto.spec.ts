import { UpdateScooterDto } from './update-scooter.dto';

describe('UpdateScooterDto', () => {
  it('should be defined', () => {
    expect(new UpdateScooterDto()).toBeDefined();
  });
});
