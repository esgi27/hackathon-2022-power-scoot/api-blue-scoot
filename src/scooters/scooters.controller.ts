import { Controller, Body, Post } from '@nestjs/common';
import { ScootersService } from './scooters.service';
import { UpdateScooterDto } from './dto/update-scooter.dto';
import { ScootersHistoryService } from './scooters-history.service';

@Controller('scooters')
export class ScootersController {
    constructor(
        private serviceScooter: ScootersService,
        private serviceHistory: ScootersHistoryService,
    ) {}

    @Post()
    async update(@Body() scooter: UpdateScooterDto) {
        const scooterFind = await this.serviceScooter.findByScooterId(
            scooter.scootId,
        );
        if (!scooterFind) {
            this.serviceScooter.createScooter(scooter);
        }
        return this.serviceHistory.insertHistory(scooter);
    }
    /**
     * recup scoot selon le scoot id
     * si il existe pas insert une entité dans tab scooter
     * insertion scooter history
     */
}
