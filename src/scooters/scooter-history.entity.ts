import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Scooter } from './scooter.entity';

@Entity('scoot_history')
export class ScooterHistory {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'scooterId' })
    scooterId: number;

    @Column({ name: 'battery_level' })
    battery_level: number;

    @Column({ name: 'longitude' })
    longitude: number;

    @Column({ name: 'latitude' })
    latitude: number;

    @Column({ name: 'created_at' })
    createdAt: string;

    @ManyToOne(() => Scooter, (scooter) => scooter.id)
    scooter: Scooter;
}
