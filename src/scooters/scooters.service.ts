import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Scooter } from './scooter.entity';
import { UpdateScooterDto } from './dto/update-scooter.dto';

@Injectable()
export class ScootersService {
    constructor(
        @InjectRepository(Scooter)
        private scootersRepository: Repository<Scooter>,
    ) {}

    async createScooter(scooterData: UpdateScooterDto) {
        const scooter = this.scootersRepository.create();
        scooter.scootId = scooterData.scootId;
        return this.scootersRepository.save(scooter);
    }

    async findByScooterId(scootId: string): Promise<Scooter> {
        const scooters = await this.scootersRepository.find({
            where: { scootId: scootId },
        });
        return scooters[0];
    }
}
