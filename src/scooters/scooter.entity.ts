import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { ScooterHistory } from './scooter-history.entity';

@Entity('scooter')
export class Scooter {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'created_at' })
    createdAt: Date;

    @Column({ name: 'scoot_id' })
    scootId: string;

    @OneToMany(() => ScooterHistory, (scootHistory) => scootHistory.scooter)
    scootHistories: ScooterHistory[];
}
