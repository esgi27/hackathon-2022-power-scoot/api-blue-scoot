import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Scooter } from './scooter.entity';
import { UpdateScooterDto } from './dto/update-scooter.dto';
import { ScooterHistory } from './scooter-history.entity';
import { ScootersService } from './scooters.service';

@Injectable()
export class ScootersHistoryService {
    constructor(
        @InjectRepository(ScooterHistory)
        private scootersHistoryRepository: Repository<ScooterHistory>,
        private scootersService: ScootersService,
    ) {}

    async insertHistory(scooterData: UpdateScooterDto) {
        let history = this.scootersHistoryRepository.create();
        if (
            !scooterData.batteryLevel ||
            !scooterData.latitude ||
            !scooterData.longitude
        ) {
            history = await this.findMostRecentHistoryByScootId(
                scooterData.scootId,
            );
        } else {
            history.scooterId = (
                await this.scootersService.findByScooterId(scooterData.scootId)
            ).id;

            history.battery_level = scooterData.batteryLevel;
            history.latitude = scooterData.latitude;
            history.longitude = scooterData.longitude;
        }

        return this.scootersHistoryRepository.save(history);
    }

    private async findMostRecentHistoryByScootId(
        scootId: string,
    ): Promise<ScooterHistory> {
        const scooterId = (await this.getScooterByScooterId(scootId)).id;
        const scooterHistory = await this.scootersHistoryRepository.find({
            where: { scooterId: scooterId },
        });
        return scooterHistory[scooterHistory.length - 1];
    }

    private async getScooterByScooterId(scootId: string): Promise<Scooter> {
        return await this.scootersService.findByScooterId(scootId);
    }
}
