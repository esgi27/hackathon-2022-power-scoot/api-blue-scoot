import { Module } from '@nestjs/common';
import { ScootersService } from './scooters.service';
import { ScootersController } from './scooters.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Scooter } from './scooter.entity';
import { ScootersHistoryService } from './scooters-history.service';
import { ScooterHistory } from './scooter-history.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Scooter, ScooterHistory])],
    providers: [ScootersService, ScootersHistoryService],
    controllers: [ScootersController],
})
export class ScootersModule {}
