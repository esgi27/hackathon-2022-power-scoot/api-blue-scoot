import { Module } from '@nestjs/common';
import { ScootersModule } from './scooters/scooters.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from 'dotenv';

config();

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: process.env.DB_HOST,
            port: parseInt(process.env.DB_PORT),
            username: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE,
            entities: ['dist/**/**.entity{.ts,.js}'],
            synchronize: false,
        }),
        ScootersModule,
    ],
})
export class AppModule {}
