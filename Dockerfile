FROM node:16-alpine AS base

WORKDIR /app
COPY package*.json ./
COPY tsconfig*.json ./
COPY ormconfig.json .
RUN npm i
ADD . .
CMD npm run start
